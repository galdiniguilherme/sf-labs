import defs
import pika
import json
 
#Consumer class
class Consumer():

    #start class
    def __init__(self, host):
        self._host = host
    
    def RunApplication(self):
      
      #data for consumer be able to access rabbitmq
      credentials = pika.PlainCredentials("guest", "guest")

      connection_parms = pika.ConnectionParameters('localhost', credentials=credentials)

      connection = pika.BlockingConnection(connection_parms)

      channel = connection.channel()

      channel.queue_declare(queue='hello')

      channel.basic_consume(queue='hello', on_message_callback=defs.return_msg)
      
      channel.start_consuming()

#start class
threads = Consumer("guest")

#call start function
threads.RunApplication()