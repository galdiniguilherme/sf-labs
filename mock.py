from unittest.mock import patch
import pika

#print
def function_output(channel, method, properties, body):
    print("Msg consumed:", body)

#where msg was published
@patch("pika.BlockingConnection", spec=pika.BlockingConnection)
def mock_publish(mock_conn):
    def side_effect_publish(exchange, routing_key, body):
        print(f"Msg published to {routing_key}:", body)

    mock_conn.return_value.channel.return_value.basic_publish.side_effect = side_effect_publish

    # data to stabilish connection to rabbitmq
    credentials = pika.PlainCredentials('guest', 'guest')
    connection = pika.BlockingConnection(pika.ConnectionParameters("localhost"), credentials=credentials)

    channel = connection.channel()

    #publish
    channel.basic_publish(exchange='',
                    routing_key="hello",
                    body="###Returned 0 to stabilish connetion with rabbitmq, message published###")

    connection.close()

# consume
@patch("pika.BlockingConnection", spec=pika.BlockingConnection)
def mock_consume(mock_conn):
    def side_effect_consume():
        function_output(mock_conn.return_value.channel, None, None, '###Returned 0 to stabilish connection for consuming###')

    mock_conn.return_value.channel.return_value.start_consuming.side_effect = side_effect_consume

    # execute consuming code
    connection = pika.BlockingConnection(pika.ConnectionParameters("localhost"))
    channel = connection.channel()
    channel.basic_consume("hello", on_message_callback=function_output)
    channel.start_consuming()

    connection.close()

#unit tests -> publish and consume
mock_publish()
mock_consume()