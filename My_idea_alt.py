import requests as req
import subprocess as sub
from defs import *
import os

link = 'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/d33156e1-5344-43bd-881b-375159b67850/payload.json?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220711%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220711T162522Z&X-Amz-Expires=86400&X-Amz-Signature=b40250fb48198139b3f3a80900d30d2c3f4020a1e64ba1590bf5d32d194ecb4d&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22payload.json%22&x-id=GetObject'
arq = req.get(link)

new_json = json.loads(arq.text)

count = 0
for itens in new_json:
     sec = itens['frame_seconds_index']
     cmd = [r'C:\ffmpeg-5.0.1-full_build\bin\ffmpeg.exe', '-i', str(itens['video_ref']), '-vf', f'select=eq(n\,{str(sec)})', '-n', '-vframes', '1', r'C:\Users\Usuario\Desktop\PyProjects\Schedule\frames\out%d.png' % (count + 1)] 
     sub.call(cmd)
     count += 1
   
for image, effect in zip(os.listdir(r'C:\Users\Usuario\Desktop\PyProjects\Schedule\frames'), new_json):

      img = cv2.imread(os.path.join(r'C:\Users\Usuario\Desktop\PyProjects\Schedule\frames', image))
      frame_effect = effect['op_type']

      if 'noise' in frame_effect:
            source = noise('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/' + image, 0.4)
            cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/', image), source)

      if 'grayscale' in frame_effect:
            source = grey_scale('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/' + image)
            cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/', image), source)

      if 'random_rotation' in frame_effect:
            source = random_rotation('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/' + image)
            cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/', image), source)

      if 'flip' in frame_effect:
            source = mirror('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/' + image)
            cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/frames/', image), source)
      