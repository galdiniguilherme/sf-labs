import cv2
import os
from defs import *

class OpenCV():
    
    def __init__(self):
        pass

    def start(self):

        #open all frames extracted with ffmpeg.py
        for image in os.listdir(r'C:\Users\Usuario\Desktop\PyProjects\Schedule\Frames_extracted_with_class'):
            
            #receive function return the last index of body
            r = receive()
            new_r = json.loads(r)

            #acessing dict index by key
            effect = new_r['op_type']

            #verifyng if the one effect is contained in 'op_type'
            if 'noise' in effect:
                    source = noise('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/' + image, 0.7)
                    cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/', image), source)

            if 'grayscale' in effect:
                    source = grey_scale('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/' + image)
                    cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/', image), source)

            if 'random_rotation' in effect:
                    source = random_rotation('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/' + image)
                    cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/', image), source)

            if 'flip' in effect:
                    source = mirror('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/' + image)
                    cv2.imwrite(os.path.join('C:/Users/Usuario/Desktop/PyProjects/Schedule/Frames_extracted_with_class/', image), source)

#start OpenCV
frame = OpenCV()
