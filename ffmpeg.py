import subprocess as sub
import Producer
from defs import *
import json
import OpenCV

#calling producer, for voiding any conexion problems
start = Producer.Publisher.RunApplication

class FFmpeg():

        def __init__(self, file_path):
                self._file_path = file_path

        def call_script(self):

                count = 0
                while count < 1000:
                        try:
                                r = receive()
                                new_r = json.loads(r)

                                video_name = new_r['video_ref']
                                video_sec = new_r['frame_seconds_index']

                                cmd = [r'C:\ffmpeg-5.0.1-full_build\bin\ffmpeg.exe', '-i', f'{video_name}', '-vf', f'select=eq(n\,{video_sec})', '-vframes', '1', r'C:\Users\Usuario\Desktop\PyProjects\Schedule\Frames_extracted_with_class\out%d.png' % (count + 1)]
                                sub.call(cmd, shell=True)
                                count += 1

                        except AttributeError:
                                break

#start FFmpeg and call OpenCV
file = FFmpeg('C:/Users/Usuario/Desktop/PyProjects/Schedule/Consumer.py')
file.call_script()
#starting frames treatment
x = OpenCV.OpenCV.start(file)


