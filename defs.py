import cv2
from scipy.ndimage import rotate
from random import randint
import numpy as np
import json
import pika

def return_msg(channel, method_frame, properties, body):
    print(json.loads(body))

def random_rotation(path):
    image_path = path 
    angle_rand = randint(0, 180) #random angle, in each execution
    source = cv2.imread(image_path)
    rotated = rotate(source, angle_rand)

    return rotated

def mirror(path):
    image_path = path
    image_read = cv2.imread(image_path)
    image_mirrowed = np.fliplr(image_read)

    return image_mirrowed

def grey_scale(path):
    image_path = path
    image_read = cv2.imread(image_path)
    image_grayScaled = cv2.cvtColor(image_read, cv2.COLOR_BGR2GRAY)

    return image_grayScaled

def noise(path, amount):
    image = path
    image_read = cv2.imread(image)
    output = image_read.copy()

    if len(image_read.shape) == 2:
        black = 0
        white = 255            
    else:
        colorspace = image_read.shape[2]
        if colorspace == 3: 
            black = np.array([0, 0, 0], dtype='uint8')
            white = np.array([255, 255, 255], dtype='uint8')
        else:  # RGBA
            black = np.array([0, 0, 0, 255], dtype='uint8')
            white = np.array([255, 255, 255, 255], dtype='uint8')

    probs = np.random.random(output.shape[:2])
    output[probs < (amount / 2)] = black
    output[probs > 1 - (amount / 2)] = white
    
    return output

#receive function consume the published message in rabbitmq
def receive():

    credentials = pika.PlainCredentials("guest", "guest")
    connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost", port="5672", credentials=credentials ))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    method_frame, header_frame, body = channel.basic_get(queue = 'hello')     

    channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    connection.close() 

    return body

def file_name():

  list = []
  while True:
    try:
      x = receive()
      new_x = json.loads(x)
      list.append(new_x['video_ref'])

    except AttributeError:
      return list

def frame_seconds_index():

  list = []
  while True:
    try:
      x = receive()
      new_x = json.loads(x)
      list.append(new_x['frame_seconds_index'])

    except AttributeError:
      return list

def op_type():

  list = []
  while True:
    try:
      x = receive()
      new_x = json.loads(x)
      list.append(new_x['op_type'])

    except AttributeError:
      return list