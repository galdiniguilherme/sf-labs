import pika
import pandas as pd
import json

link = 'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/d33156e1-5344-43bd-881b-375159b67850/payload.json?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220711%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220711T205613Z&X-Amz-Expires=86400&X-Amz-Signature=9d1b15b468ce9f6cf1e69b04b53f1c31dd86d59947723ffde1837e8f7ad311bb&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22payload.json%22&x-id=GetObject'
df = pd.read_json(link)

#class
class Publisher():

    #costructor
    def __init__(self, host):
        self._host = host

    #data needed by access rabbitmq
    def RunApplication(self):
        credentials = pika.PlainCredentials("guest", "guest")
        connection = pika.BlockingConnection( pika.ConnectionParameters( host="localhost",port="5672",credentials=credentials ))
        channel = connection.channel()

        #setting how data will be printed after consume
        message = df.to_json(orient='records')
        payloads = json.loads(message)
        for payload in payloads:
            msg_formated = json.dumps(payload, sort_keys=False, indent=None)
            
            #address
            channel.queue_declare(queue='hello')
            channel.basic_publish(exchange='',
                        routing_key='hello',
                        body=msg_formated)

        connection.close()

#start class
publish = Publisher("guest")
#call start function
publish.RunApplication()